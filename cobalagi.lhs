> circleArea a = pi*a*a
> areaOfCirclesGeneral [] = 0
> areaOfCirclesGeneral (x:xs) = circleArea x + areaOfCirclesGeneral xs

> data Shape = Circle Float Float Float | Rectangle Float Float Float Float deriving (Show)

> surface (Circle _ _ r) = pi * r * r
> surface (Rectangle x1 y1 x2 y2) = (abs $ x1 - x2) * (abs $ y1 - y2)

> length' xs = sum [1 | a <- xs]

> lengthRecursion [] = 0
> lengthRecursion (x:xs) = 1 + lengthRecursion xs

> sumRecursion [] = 0
> sumRecursion (x:xs) = x + sumRecursion xs

> max' a b | a > b = a | otherwise = b


> listAbstract operation [] = 0
> listAbstract operation (x:xs) = x `operation` listAbstract operation xs

> bmiTell bmi  
>   | bmi <= 18.5 = "You're underweight, you emo, you!"  
>   | bmi <= 25.0 = "You're supposedly normal. Pffft, I bet you're ugly!"  
>   | bmi <= 30.0 = "You're fat! Lose some weight, fatty!"  
>   | otherwise   = "You're a whale, congratulations!"