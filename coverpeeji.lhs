> data Expr = C Float | Expr :+ Expr | Expr :- Expr | Expr :* Expr | Expr :/ Expr
>   | V String | Let String Expr Expr
>   deriving Show

> subst :: String -> Expr -> Expr -> Expr
> subst v0 e0 (V v1) = if (v0 == v1) then e0 else (V v1)
> subst _ _ (C c) = (C c)
> subst v0 e0 (e1 :+ e2) = subst v0 e0 e1 :+ subst v0 e0 e2
> subst v0 e0 (e1 :- e2) = subst v0 e0 e1 :- subst v0 e0 e2
> subst v0 e0 (e1 :* e2) = subst v0 e0 e1 :* subst v0 e0 e2
> subst v0 e0 (e1 :/ e2) = subst v0 e0 e1 :/ subst v0 e0 e2
> subst v0 e0 (Let v1 e1 e2) = Let v1 e1 (subst v0 e0 e2) -- perlu koreksi ???

> evaluate :: Expr -> Float
> evaluate (C x) = x
> evaluate (V _) = 0.0
> evaluate (e1 :+ e2) = evaluate e1 + evaluate e2
> evaluate (e1 :- e2) = evaluate e1 - evaluate e2
> evaluate (e1 :* e2) = evaluate e1 * evaluate e2
> evaluate (e1 :/ e2) = evaluate e1 / evaluate e2
> evaluate (Let v e0 e1) = evaluate (subst v e0 e1)

-- mapExp (+2) (C 2)
-- C 4.0

> mapExp f (C x) = C (f x)
> mapExp f (V _) = C (f 0.0)
> mapExp f (e1 :+ e2) = mapExp f e1 :+ mapExp f e2
> mapExp f (e1 :* e2) = mapExp f e1 :* mapExp f e2
> mapExp f (e1 :- e2) = mapExp f e1 :- mapExp f e2
> mapExp f (e1 :/ e2) = mapExp f e1 :/ mapExp f e2
