>>> 3 * 2
6

>>> easy x y z = x * (y + z)
>>> easy 1 2 3
5

>>> 5 == 6
False

>>> div 92 10
9
>>> 92 `div` 10
9

>>> nums = [1,2,3,4]
>>> let nums2 = [1,2,3,4]
>>> nums
>>> nums2

>>> "hello" ++ "world"
"helloworld"

>>> [4] ++ [1,2,3]
[4,1,2,3]

>>> take 10 [2,4..]
[2,4,6,8,10,12,14,16,18,20]

>>> [x*2 | x <- [1..10], x `mod` 2 == 0]
[4,8,12,16,20]

>>> let triangles = [(a,b,c) | c <-[1..10], b <-[1..10], a<-[1..10], b < c, a < c, c*c == a*a + b*b, c+a+b == 24]
>>> triangles
[(8,6,10),(6,8,10)]

>>> :t (==)
(==) :: Eq a => a -> a -> Bool

>>> :t (show)
(show) :: Show a => a -> String

>>> :t (read)
(read) :: Read a => String -> a
>>> read "5" :: Int
5



>>> circleArea a = pi*a*a
>>> :t circleArea
>>> areaOfCircles a b c =  circleArea a + circleArea b + circleArea c
>>> :t areaOfCircles
circleArea :: Floating a => a -> a
areaOfCircles :: Floating a => a -> a -> a -> a

>>> areaOfCircles 2 3 4
91.106186954104

>>> areaOfCirclesGeneral [] = 0
>>> areaOfCirclesGeneral (x:xs) = circleArea x + areaOfCirclesGeneral xs
>>> :t areaOfCirclesGeneral



