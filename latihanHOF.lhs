> length' [] = 0
> length' (x:xs) = 1 + length xs

> beOne x = 1
> lengthsummap xs = sum (map beOne xs)

> plusDua xs = map (+1) (map (+1) xs)

> sumToN n = foldr (+) 0 (map (^2) [1..n])


> mystery xs = foldl (++) [] (map sing xs) where sing x = [x]

> flip' a b c = a c b

> lengthSUMmap xs = sum (map (\x -> 1) xs)