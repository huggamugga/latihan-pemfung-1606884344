> plus1 xs = map (+1) xs

> plus2Above3 xs = map (+2) (filter(>3) xs)

> plus3LOTuple xs = map (\(x,_) -> x+3) xs

> plus4 xs = map (\(x,y)->x+4) (filter (\(x,y) -> x+y < 5) xs)

> listCompPlus3 lst = [x+3 | x<-lst]

> filter7 lst = [x | x<-lst, x>7]

> filter3Map xs = [x+y | (x,y) <- xs, (x+y > 3)]

> xplusykombinasi xs ys = (map (\x -> (map (\y -> x +y) ys)) xs)