> data Tree a = Leaf a | Branch (Tree a) (Tree a) deriving (Show, Eq, Ord)

> maptree func (Leaf a) = Leaf (func a)
> maptree func (Branch a b) = Branch (maptree func a) (maptree func b)

> foldPohon _ (Leaf a) = a
> foldPohon fungsiFold (Branch a b) = (foldPohon fungsiFold a) `fungsiFold` (foldPohon fungsiFold b)

> foldFungsidanFunc fungsiMap _ (Leaf a) = fungsiMap a
> foldFungsidanFunc fungsiMap fungsiFold (Branch a b) = (foldFungsidanFunc fungsiMap fungsiFold a) `fungsiFold` (foldFungsidanFunc fungsiMap fungsiFold b)

-- foldFungsidanFunc (+2) (+) (Branch (Leaf 3) (Leaf 4))
-- 