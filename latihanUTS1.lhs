-- arithmetic expressions

> data Expr = C Float
>   | Expr :+ Expr
>   | Expr :- Expr
>   | Expr :* Expr
>   | Expr :/ Expr deriving (Show)

> evaluate::Expr -> Float
> evaluate (C x) = x
> evaluate (C a :+ C b) = evaluate (C a) + evaluate (C b)
> evaluate (C a :* C b) = evaluate (C a) * evaluate (C b)
> evaluate (C a :/ C b) = evaluate (C a) / evaluate (C b)
> evaluate (C a :- C b) = evaluate (C a) - evaluate (C b)

> mapBiasa f [] = []
> mapBiasa f (x:xs) = f x : mapBiasa f xs

-- > mapEks f [] = []
-- > mapEks f (x:xs) = f x : mapEks 
-- misal punya ekspresi : C 10, jika diberikan (\x->x+10) ekspresinya menjadi C 20
-- misal punya ekspresi : (C 10 :+ C 20), ekspresi jadi C 20 :+ C 30

> mapEkspresi f (C konst) = C (f konst)

-- base case, ketika hanya C konst yang masuk berarti harus return C (f konst)::Expr

> mapEkspresi f (C konst1 :+ C konst2) = mapEkspresi f (C (f konst1)) :+ mapEkspresi f (C (f konst2))
> mapEkspresi f (C konst1 :- C konst2) = C (f konst1) :- C (f konst2)
> mapEkspresi f (C konst1 :* C konst2) = C (f konst1) :* C (f konst2)
> mapEkspresi f (C konst1 :/ C konst2) = C (f konst1) :/ C (f konst2)
