-- shapes

> data Shape = Circle Float Float Float deriving (Show)


> surface (Circle _ _ r) = pi * r * r


> data Person = Person {
>   firstName::String,
>   lastName::String,
>   age::Int,
>   height::Float,
>   phoneNumber::String,
>   flavor::String
> } deriving (Show)