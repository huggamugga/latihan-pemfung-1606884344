> xPlusYXGreaterThanY xs ys = concat (map (\x -> map(\y->x+y) (filter (\y -> x>y) ys)) xs)

> divisor n = [x | x<-[1..], n `mod` x == 0]

> quickListCompre [] = []
> quickListCompre (x:xs) = quickListCompre[y | y <- xs, y < x] ++ [x] ++ quickListCompre[y | y <- xs, y >=x ]

> tryplePythagoras = [(x,y,z) |z<-[1..],  x <-[z, z-1..1], y<-[z,z-1..1], x*x+y*y==z*z]