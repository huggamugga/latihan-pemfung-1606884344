-- zip together

> zipTogether [] [] = []
> zipTogether [] ys = []
> zipTogether xs [] = []
> zipTogether (x:xs) (y:ys) = (x,y) : zipTogether xs ys

-- maximum recursion

> max' [] = error "error"
> max' [x] = x
> max' (x:xs) = max x (max' xs)

-- replicate

> replicate' 0 a = []
> replicate' b a = a:replicate' (b-1) a

-- flow : a:a:a:...:[]

-- take

> take' 0 (x:xs) = []
> take' a [] = []
> take' a (x:xs) = x:take' (a-1) xs

-- elemen pertama di x:xs (x) diambil lalu diappend dengan hasil rekursi element lain

-- reverse

> reverse' [] = []
> reverse' xs = last(xs) : reverse' (take (length xs - 1) xs)

-- another reverse implementation

> reverse'' [] = []
> reverse'' (x:xs) = reverse'' (xs) ++ [x]

-- element

> elem' _ [] = False
> elem' a (x:xs)
>   | a == x = True
>   | a /= x = elem' a xs

-- memakai guard, jika a == x, akan langsung return true dan jika tidak, akan terus dilakukan proses rekursif

-- divisor'

> divisor' 0 = []
> divisor' a = filter (\x-> a `mod` x == 0) [1..a]
